const { io } = require("socket.io-client");

let socket;
socket = io("ws://192.168.31.235:3000");
var peer = new Peer(undefined, {
  path: "/peerjs",
  host: "192.168.31.235",
  port: 3000,
});

window.onload = function () {
  console.log("Strated");

  if (navigator.getUserMedia) {
    console.log("entered");
    let videoCallButton = document.getElementById("videoCallButton");
    let endCallButton = document.getElementById("endCallButton");
    let localVideo = document.getElementById("localVideo");
    let remotrVideo = document.getElementById("remotrVideo");
    videoCallButton.removeAttribute("disabled");
    let myVideoStream;

    navigator.mediaDevices
      .getUserMedia({
        audio: true,
        video: true,
      })
      .then((stream) => {
        myVideoStream = stream;
        addVideoStream(localVideo, stream);
        peer.on("call", (call) => {
          call.answer(stream);
          call.on("stream", (userVideoStream) => {
            addVideoStream(remotrVideo, userVideoStream);
          });
        });
        socket.on("user-connected", (userId) => {
          connectToNewUser(userId, stream);
        });
      });
    peer.on("open", (id) => {
      console.log("opened" + " " + id);
      socket.emit("join-room", 5, id);
    });
    const connectToNewUser = (userId, stream) => {
      const call = peer.call(userId, stream);
      call.on("stream", (userVideoStream) => {
        addVideoStream(remotrVideo, userVideoStream);
      });
    };
  }

  const addVideoStream = (video, stream) => {
    video.srcObject = stream;
    video.addEventListener("loadedmetadata", () => {
      video.play();
    });
  };
};
